"""Emoji

Available Commands:

.fleave"""

from telethon import events

import asyncio





@borg.on(events.NewMessage(pattern=r"\.(.*)", outgoing=True))

async def _(event):

    if event.fwd_from:

        return

    animation_interval = 1

    animation_ttl = range(0, 17)

    input_str = event.pattern_match.group(1)

    if input_str == "fleave":

        await event.edit(input_str)

        animation_chars = [
        
            "⬛⬛⬛\n⬛⬛⬛\n⬛⬛⬛",
            "⬛⬛⬛\n⬛🔄⬛\n⬛⬛⬛",
            "⬛⬆️⬛\n⬛🔄⬛\n⬛⬛⬛",
            "⬛⬆️↗️\n⬛🔄⬛\n⬛⬛⬛",
            "⬛⬆️↗️\n⬛🔄➡️\n⬛⬛⬛",    
            "⬛⬆️↗️\n⬛🔄➡️\n⬛⬛↘️",
            "⬛⬆️↗️\n⬛🔄➡️\n⬛⬇️↘️",
            "⬛⬆️↗️\n⬛🔄➡️\n↙️⬇️↘️",
            "⬛⬆️↗️\n⬅️🔄➡️\n↙️⬇️↘️",
            "↖️⬆️↗️\n⬅️🔄➡️\n↙️⬇️↘️",
            "**Chat Message Exported To** `.Private data base`",
            "**Chat Message Exported To** `.private data base/hacking section`",
            "**Chat Message Exported To** `.private data base/hacking section/groupchat.txt`",
            "ബൈ ദി ബൈ...😣 ഞാൻ പോകുന്നു...ഗ്രുപ്പിന്റെ നേടും തൂണ്....😋 ഞാൻ ഇല്ലാത്ത വിഷമത്തിൽ ആരും കണ്ടം നോക്കാൻ പോകരുത്...😢",
            "ബൈ ദി ബൈ...😣 ഞാൻ പോകുന്നു...ഗ്രുപ്പിന്റെ നേടും തൂണ്....😋 ഞാൻ ഇല്ലാത്ത വിഷമത്തിൽ ആരും കണ്ടം നോക്കാൻ പോകരുത്...😢"

 ]

        for i in animation_ttl:

            await asyncio.sleep(animation_interval)

            await event.edit(animation_chars[i % 17])
