"""COMMAND : .theri"""
import asyncio
from telethon import events
from telethon.tl.types import ChannelParticipantsAdmins
from uniborg.util import admin_cmd


@borg.on(admin_cmd("theri"))
async def _(event):
    if event.fwd_from:
        return
    mentions = "`നിന്റെ അമ്മയുടെ ചൊറി പിടിച്ച ഇഡ്ഡലികൂതിയിലെ തൊണ്ടൻ പയർ കന്തിൽ നിന്റെ കാവക്ക നാവ് കൊണ്ട് നക്കി തുടച്ചു അതിൽ നിന്ന് വരുന്ന മൂത്രവും കുടിച്ചു നിന്റെ അമ്മയുടെ കൊതത്തിലെ പുഴുത്ത തൊളയിലെ പുഴുക്കളെ നിന്റെ നാവ് കൊണ്ട് നക്കിഎടുത്തു അവളെ രസിപ്പിക്കെടാ അച്ചില്ക്കി പൂറി മോനെ.  എന്നിട്ട് നിന്റെ കുഞ്ഞ് പെങ്ങളുടെ മുലകളിലെ ചുമന്ന റോസാ മോട്ടു ചപ്പികൊടുക്ക്. എന്നിട്ട് നിന്റെ തന്തയുടെ വെപ്പണ്ടി വായിൽ ഇട്ട് ഊമ്പി ഊമ്പി അമ്മയെ കളിച്ച കഥയും പറഞ്ഞു ഇരുന്നോ.  നിന്റെ വീട്ടുകാർക്ക് എങ്കിലും നിന്നെക്കൊണ്ട് അങ്ങനെ ഒരു ഉപയോഗം ഉണ്ടാകട്ടെനായിന്റെ മോൻ ഇവിടെ ഉണ്ടേൽ വെറുതെ വിടില്ല ആ അണ്ടിക്ക് ഉറപ്പില്ലാത്ത ആലിങ്ങാ പൂറി മോനെ.  നിന്റെ അമ്മേടെ പൂറ്റിലെ കഥന കഥയും പറഞ്ഞു ഇനി ഗ്രൂപ്പിൽ വന്നാൽ തയോളി നിന്റെ വീട്ടിൽ കുണ്ടന്മാർ കേറി നിരങ്ങും. നിന്റെ അമ്മയോട് ചോദിക്ക്.   നല്ല ആണുങ്ങളെ വിളിച്ചു മക്കളെ ഉണ്ടാക്കിപ്പിച്ചാൽ പോരായിരുന്നോ എന്ന്.  ഇല്ലേൽ നിന്നെ പോലെ കാട്ട് കുണ്ടന്മാർക്ക് ഉണ്ടാവുമായിരുന്നില്ലല്ലോ എന്നും!'"
    chat = await event.get_input_chat()
    async for x in borg.iter_participants(chat, filter=ChannelParticipantsAdmins):
        mentions += f""
    reply_message = None
    if event.reply_to_msg_id:
        reply_message = await event.get_reply_message()
        await reply_message.reply(mentions)
    else:
        await event.reply(mentions)
    await event.delete()
